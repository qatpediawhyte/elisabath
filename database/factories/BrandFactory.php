<?php

$factory->define(App\Brand::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "description" => $faker->name,
        "type" => collect(["audio visual","hospitality","control rooms and studio",])->random(),
    ];
});
