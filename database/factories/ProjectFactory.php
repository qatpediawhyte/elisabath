<?php

$factory->define(App\Project::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "description" => $faker->name,
        "status" => collect(["ongoing","completed","awarded",])->random(),
        "client_name" => $faker->name,
        "location" => $faker->name,
    ];
});
