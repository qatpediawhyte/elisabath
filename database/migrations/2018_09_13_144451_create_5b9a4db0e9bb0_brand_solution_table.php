<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5b9a4db0e9bb0BrandSolutionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('brand_solution')) {
            Schema::create('brand_solution', function (Blueprint $table) {
                $table->integer('brand_id')->unsigned()->nullable();
                $table->foreign('brand_id', 'fk_p_13319_13320_solution_5b9a4db0e9c61')->references('id')->on('brands')->onDelete('cascade');
                $table->integer('solution_id')->unsigned()->nullable();
                $table->foreign('solution_id', 'fk_p_13320_13319_brand_so_5b9a4db0e9cb2')->references('id')->on('solutions')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_solution');
    }
}
