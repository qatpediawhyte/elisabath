<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1536642170ProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            
if (!Schema::hasColumn('projects', 'description')) {
                $table->text('description')->nullable();
                }
if (!Schema::hasColumn('projects', 'status')) {
                $table->enum('status', array('ongoing', 'completed', 'awarded'))->nullable();
                }
if (!Schema::hasColumn('projects', 'client_name')) {
                $table->string('client_name')->nullable();
                }
if (!Schema::hasColumn('projects', 'location')) {
                $table->string('location')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('status');
            $table->dropColumn('client_name');
            $table->dropColumn('location');
            
        });

    }
}
