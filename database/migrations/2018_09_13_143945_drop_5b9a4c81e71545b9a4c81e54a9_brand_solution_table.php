<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drop5b9a4c81e71545b9a4c81e54a9BrandSolutionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('brand_solution');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('brand_solution')) {
            Schema::create('brand_solution', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('brand_id')->unsigned()->nullable();
            $table->foreign('brand_id', 'fk_p_13319_13320_solution_5b974e3e42b25')->references('id')->on('brands');
                $table->integer('solution_id')->unsigned()->nullable();
            $table->foreign('solution_id', 'fk_p_13320_13319_brand_so_5b974e3e43648')->references('id')->on('solutions');
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}
