<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5b974e3e64ad0BrandSolutionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('brand_solution')) {
            Schema::create('brand_solution', function (Blueprint $table) {
                $table->integer('brand_id')->unsigned()->nullable();
                $table->foreign('brand_id', 'fk_p_13319_13320_solution_5b974e3e64b81')->references('id')->on('brands')->onDelete('cascade');
                $table->integer('solution_id')->unsigned()->nullable();
                $table->foreign('solution_id', 'fk_p_13320_13319_brand_so_5b974e3e64bc6')->references('id')->on('solutions')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_solution');
    }
}
