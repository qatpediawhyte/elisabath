<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1536839016BrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brands', function (Blueprint $table) {
            if(Schema::hasColumn('brands', 'type')) {
                $table->dropColumn('type');
            }
            
        });
Schema::table('brands', function (Blueprint $table) {
            
if (!Schema::hasColumn('brands', 'type')) {
                $table->enum('type', array('audio visual', 'hospitality', 'control rooms and studio'))->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brands', function (Blueprint $table) {
            $table->dropColumn('type');
            
        });
Schema::table('brands', function (Blueprint $table) {
                        $table->enum('type', array('audio visual', 'hospitality', 'control rooms &amp;amp; studio'))->nullable();
                
        });

    }
}
