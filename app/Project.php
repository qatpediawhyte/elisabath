<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Project
 *
 * @package App
 * @property string $title
 * @property text $description
 * @property enum $status
 * @property string $client_name
 * @property string $location
*/
class Project extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    
    protected $fillable = ['title', 'description', 'status', 'client_name', 'location'];
    protected $appends = ['image', 'image_link', 'uploaded_image'];
    protected $with = ['media'];
    

    public static $enum_status = ["ongoing" => "Ongoing", "completed" => "Completed", "awarded" => "Awarded"];

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'description' => 'max:65535|nullable',
            'status' => 'in:ongoing,completed,awarded|required',
            'client_name' => 'max:191|required',
            'location' => 'max:191|required',
            'image' => 'nullable',
            'image.*' => 'file|image|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'description' => 'max:65535|nullable',
            'status' => 'in:ongoing,completed,awarded|required',
            'client_name' => 'max:191|required',
            'location' => 'max:191|required',
            'image' => 'sometimes',
            'image.*' => 'file|image|nullable'
        ];
    }

    

    public function getImageAttribute()
    {
        return [];
    }

    public function getUploadedImageAttribute()
    {
        return $this->getMedia('image')->keyBy('id');
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute()
    {
        $image = $this->getMedia('image');
        if (! count($image)) {
            return null;
        }
        $html = [];
        foreach ($image as $file) {
            $html[] = '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
        }

        return implode('<br/>', $html);
    }
    
    
}
