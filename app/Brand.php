<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Brand
 *
 * @package App
 * @property string $title
 * @property string $logo
 * @property text $description
 * @property enum $type
*/
class Brand extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    
    protected $fillable = ['title', 'description', 'type'];
    protected $appends = ['logo', 'logo_link'];
    protected $with = ['media'];
    

    public static $enum_type = ["audio visual" => "Audio visual", "hospitality" => "Hospitality", "control rooms and studio" => "Control rooms and studio"];

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'logo' => 'file|image|required',
            'description' => 'max:65535|nullable',
            'type' => 'in:audio visual,hospitality,control rooms and studio|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'logo' => 'nullable',
            'description' => 'max:65535|nullable',
            'type' => 'in:audio visual,hospitality,control rooms and studio|required'
        ];
    }

    

    public function getLogoAttribute()
    {
        return $this->getFirstMedia('logo');
    }

    /**
     * @return string
     */
    public function getLogoLinkAttribute()
    {
        $file = $this->getFirstMedia('logo');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }
    
    
}
