<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class News
 *
 * @package App
 * @property string $title
 * @property string $image
 * @property text $description
*/
class News extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    
    protected $table = "news";
    
    protected $fillable = ['title', 'description'];
    protected $appends = ['image', 'image_link'];
    protected $with = ['media'];
    

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'image' => 'file|image|nullable',
            'description' => 'max:65535|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'image' => 'nullable',
            'description' => 'max:65535|nullable'
        ];
    }

    

    public function getImageAttribute()
    {
        return $this->getFirstMedia('image');
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute()
    {
        $file = $this->getFirstMedia('image');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }
    
    
}
