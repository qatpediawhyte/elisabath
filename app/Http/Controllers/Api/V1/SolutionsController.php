<?php

namespace App\Http\Controllers\Api\V1;

use App\Solution;
use App\Http\Controllers\Controller;
use App\Http\Resources\Solution as SolutionResource;
use App\Http\Requests\Admin\StoreSolutionsRequest;
use App\Http\Requests\Admin\UpdateSolutionsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;


class SolutionsController extends Controller
{
    public function index()
    {
        

        return new SolutionResource(Solution::with(['brand'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('solution_view')) {
            return abort(401);
        }

        $solution = Solution::with(['brand'])->findOrFail($id);

        return new SolutionResource($solution);
    }

    public function store(StoreSolutionsRequest $request)
    {
        if (Gate::denies('solution_create')) {
            return abort(401);
        }

        $solution = Solution::create($request->all());
        $solution->brand()->sync($request->input('brand', []));
        if ($request->hasFile('icon')) {
            $solution->addMedia($request->file('icon'))->toMediaCollection('icon');
        }if ($request->hasFile('image')) {
            $solution->addMedia($request->file('image'))->toMediaCollection('image');
        }if ($request->hasFile('front_image')) {
            $solution->addMedia($request->file('front_image'))->toMediaCollection('front_image');
        }

        return (new SolutionResource($solution))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateSolutionsRequest $request, $id)
    {
        if (Gate::denies('solution_edit')) {
            return abort(401);
        }

        $solution = Solution::findOrFail($id);
        $solution->update($request->all());
        $solution->brand()->sync($request->input('brand', []));
        if (! $request->input('icon') && $solution->getFirstMedia('icon')) {
            $solution->getFirstMedia('icon')->delete();
        }if (! $request->input('image') && $solution->getFirstMedia('image')) {
            $solution->getFirstMedia('image')->delete();
        }if (! $request->input('front_image') && $solution->getFirstMedia('front_image')) {
            $solution->getFirstMedia('front_image')->delete();
        }
        if ($request->hasFile('icon')) {
            $solution->addMedia($request->file('icon'))->toMediaCollection('icon');
        }if ($request->hasFile('image')) {
            $solution->addMedia($request->file('image'))->toMediaCollection('image');
        }if ($request->hasFile('front_image')) {
            $solution->addMedia($request->file('front_image'))->toMediaCollection('front_image');
        }

        return (new SolutionResource($solution))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('solution_delete')) {
            return abort(401);
        }

        $solution = Solution::findOrFail($id);
        $solution->delete();

        return response(null, 204);
    }
}
