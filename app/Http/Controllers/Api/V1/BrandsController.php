<?php

namespace App\Http\Controllers\Api\V1;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Http\Resources\Brand as BrandResource;
use App\Http\Requests\Admin\StoreBrandsRequest;
use App\Http\Requests\Admin\UpdateBrandsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;


class BrandsController extends Controller
{
    public function index()
    {
        

        return new BrandResource(Brand::with([])->get());
    }

    public function show($id)
    {
        if (Gate::denies('brand_view')) {
            return abort(401);
        }

        $brand = Brand::with([])->findOrFail($id);

        return new BrandResource($brand);
    }

    public function store(StoreBrandsRequest $request)
    {
        if (Gate::denies('brand_create')) {
            return abort(401);
        }

        $brand = Brand::create($request->all());
        
        if ($request->hasFile('logo')) {
            $brand->addMedia($request->file('logo'))->toMediaCollection('logo');
        }

        return (new BrandResource($brand))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateBrandsRequest $request, $id)
    {
        if (Gate::denies('brand_edit')) {
            return abort(401);
        }

        $brand = Brand::findOrFail($id);
        $brand->update($request->all());
        
        if (! $request->input('logo') && $brand->getFirstMedia('logo')) {
            $brand->getFirstMedia('logo')->delete();
        }
        if ($request->hasFile('logo')) {
            $brand->addMedia($request->file('logo'))->toMediaCollection('logo');
        }

        return (new BrandResource($brand))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('brand_delete')) {
            return abort(401);
        }

        $brand = Brand::findOrFail($id);
        $brand->delete();

        return response(null, 204);
    }
}
