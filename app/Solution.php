<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Solution
 *
 * @package App
 * @property string $title
 * @property string $icon
 * @property string $image
 * @property text $description
 * @property string $front_image
*/
class Solution extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    
    protected $fillable = ['title', 'description'];
    protected $appends = ['icon', 'icon_link', 'image', 'image_link', 'front_image', 'front_image_link'];
    protected $with = ['media'];
    

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'icon' => 'file|image|required',
            'image' => 'file|image|required',
            'description' => 'max:65535|nullable',
            'front_image' => 'file|image|required',
            'brand' => 'array|nullable',
            'brand.*' => 'integer|exists:brands,id|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'icon' => 'nullable',
            'image' => 'nullable',
            'description' => 'max:65535|nullable',
            'front_image' => 'nullable',
            'brand' => 'array|nullable',
            'brand.*' => 'integer|exists:brands,id|max:4294967295|nullable'
        ];
    }

    

    public function getIconAttribute()
    {
        return $this->getFirstMedia('icon');
    }

    /**
     * @return string
     */
    public function getIconLinkAttribute()
    {
        $file = $this->getFirstMedia('icon');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }

    public function getImageAttribute()
    {
        return $this->getFirstMedia('image');
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute()
    {
        $file = $this->getFirstMedia('image');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }

    public function getFrontImageAttribute()
    {
        return $this->getFirstMedia('front_image');
    }

    /**
     * @return string
     */
    public function getFrontImageLinkAttribute()
    {
        $file = $this->getFirstMedia('front_image');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }
    
    public function brand()
    {
        return $this->belongsToMany(Brand::class, 'brand_solution')->withTrashed();
    }
    
    
}
