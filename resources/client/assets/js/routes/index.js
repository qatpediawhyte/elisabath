import Vue from 'vue'
import VueRouter from 'vue-router'

import ChangePassword from '../components/ChangePassword.vue'
import PermissionsIndex from '../components/cruds/Permissions/Index.vue'
import PermissionsCreate from '../components/cruds/Permissions/Create.vue'
import PermissionsShow from '../components/cruds/Permissions/Show.vue'
import PermissionsEdit from '../components/cruds/Permissions/Edit.vue'
import RolesIndex from '../components/cruds/Roles/Index.vue'
import RolesCreate from '../components/cruds/Roles/Create.vue'
import RolesShow from '../components/cruds/Roles/Show.vue'
import RolesEdit from '../components/cruds/Roles/Edit.vue'
import UsersIndex from '../components/cruds/Users/Index.vue'
import UsersCreate from '../components/cruds/Users/Create.vue'
import UsersShow from '../components/cruds/Users/Show.vue'
import UsersEdit from '../components/cruds/Users/Edit.vue'
import ProjectsIndex from '../components/cruds/Projects/Index.vue'
import ProjectsCreate from '../components/cruds/Projects/Create.vue'
import ProjectsShow from '../components/cruds/Projects/Show.vue'
import ProjectsEdit from '../components/cruds/Projects/Edit.vue'
import BrandsIndex from '../components/cruds/Brands/Index.vue'
import BrandsCreate from '../components/cruds/Brands/Create.vue'
import BrandsShow from '../components/cruds/Brands/Show.vue'
import BrandsEdit from '../components/cruds/Brands/Edit.vue'
import SolutionsIndex from '../components/cruds/Solutions/Index.vue'
import SolutionsCreate from '../components/cruds/Solutions/Create.vue'
import SolutionsShow from '../components/cruds/Solutions/Show.vue'
import SolutionsEdit from '../components/cruds/Solutions/Edit.vue'
import NewsIndex from '../components/cruds/News/Index.vue'
import NewsCreate from '../components/cruds/News/Create.vue'
import NewsShow from '../components/cruds/News/Show.vue'
import NewsEdit from '../components/cruds/News/Edit.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/change-password', component: ChangePassword, name: 'auth.change_password' },
    { path: '/permissions', component: PermissionsIndex, name: 'permissions.index' },
    { path: '/permissions/create', component: PermissionsCreate, name: 'permissions.create' },
    { path: '/permissions/:id', component: PermissionsShow, name: 'permissions.show' },
    { path: '/permissions/:id/edit', component: PermissionsEdit, name: 'permissions.edit' },
    { path: '/roles', component: RolesIndex, name: 'roles.index' },
    { path: '/roles/create', component: RolesCreate, name: 'roles.create' },
    { path: '/roles/:id', component: RolesShow, name: 'roles.show' },
    { path: '/roles/:id/edit', component: RolesEdit, name: 'roles.edit' },
    { path: '/users', component: UsersIndex, name: 'users.index' },
    { path: '/users/create', component: UsersCreate, name: 'users.create' },
    { path: '/users/:id', component: UsersShow, name: 'users.show' },
    { path: '/users/:id/edit', component: UsersEdit, name: 'users.edit' },
    { path: '/projects', component: ProjectsIndex, name: 'projects.index' },
    { path: '/projects/create', component: ProjectsCreate, name: 'projects.create' },
    { path: '/projects/:id', component: ProjectsShow, name: 'projects.show' },
    { path: '/projects/:id/edit', component: ProjectsEdit, name: 'projects.edit' },
    { path: '/brands', component: BrandsIndex, name: 'brands.index' },
    { path: '/brands/create', component: BrandsCreate, name: 'brands.create' },
    { path: '/brands/:id', component: BrandsShow, name: 'brands.show' },
    { path: '/brands/:id/edit', component: BrandsEdit, name: 'brands.edit' },
    { path: '/solutions', component: SolutionsIndex, name: 'solutions.index' },
    { path: '/solutions/create', component: SolutionsCreate, name: 'solutions.create' },
    { path: '/solutions/:id', component: SolutionsShow, name: 'solutions.show' },
    { path: '/solutions/:id/edit', component: SolutionsEdit, name: 'solutions.edit' },
    { path: '/news', component: NewsIndex, name: 'news.index' },
    { path: '/news/create', component: NewsCreate, name: 'news.create' },
    { path: '/news/:id', component: NewsShow, name: 'news.show' },
    { path: '/news/:id/edit', component: NewsEdit, name: 'news.edit' },
]

export default new VueRouter({
    mode: 'history',
    base: '/admin',
    routes
})
