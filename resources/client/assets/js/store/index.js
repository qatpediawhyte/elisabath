import Vue from 'vue'
import Vuex from 'vuex'
import Alert from './modules/alert'
import ChangePassword from './modules/change_password'
import Rules from './modules/rules'
import PermissionsIndex from './modules/Permissions'
import PermissionsSingle from './modules/Permissions/single'
import RolesIndex from './modules/Roles'
import RolesSingle from './modules/Roles/single'
import UsersIndex from './modules/Users'
import UsersSingle from './modules/Users/single'
import ProjectsIndex from './modules/Projects'
import ProjectsSingle from './modules/Projects/single'
import BrandsIndex from './modules/Brands'
import BrandsSingle from './modules/Brands/single'
import SolutionsIndex from './modules/Solutions'
import SolutionsSingle from './modules/Solutions/single'
import NewsIndex from './modules/News'
import NewsSingle from './modules/News/single'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        Alert,
        ChangePassword,
        Rules,
        PermissionsIndex,
        PermissionsSingle,
        RolesIndex,
        RolesSingle,
        UsersIndex,
        UsersSingle,
        ProjectsIndex,
        ProjectsSingle,
        BrandsIndex,
        BrandsSingle,
        SolutionsIndex,
        SolutionsSingle,
        NewsIndex,
        NewsSingle,
    },
    strict: debug,
})
