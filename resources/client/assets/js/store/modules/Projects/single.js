function initialState() {
    return {
        item: {
            id: null,
            title: null,
            description: null,
            status: null,
            client_name: null,
            location: null,
            image: [],
            uploaded_image: [],
        },
        
        statusEnum: [ { value: 'ongoing', label: 'Ongoing' }, { value: 'completed', label: 'Completed' }, { value: 'awarded', label: 'Awarded' }, ],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    
    statusEnum: state => state.statusEnum,
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (! _.isEmpty(state.item.status) && typeof state.item.status === 'object') {
                params.set('status', state.item.status.value)
            }
            params.set('uploaded_image', state.item.uploaded_image.map(o => o['id']))

            axios.post('/api/v1/projects', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (! _.isEmpty(state.item.status) && typeof state.item.status === 'object') {
                params.set('status', state.item.status.value)
            }
            params.set('uploaded_image', state.item.uploaded_image.map(o => o['id']))

            axios.post('/api/v1/projects/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/projects/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        
    },
    
    setTitle({ commit }, value) {
        commit('setTitle', value)
    },
    setDescription({ commit }, value) {
        commit('setDescription', value)
    },
    setStatus({ commit }, value) {
        commit('setStatus', value)
    },
    setClient_name({ commit }, value) {
        commit('setClient_name', value)
    },
    setLocation({ commit }, value) {
        commit('setLocation', value)
    },
    setImage({ commit }, value) {
        commit('setImage', value)
    },
    destroyImage({ commit }, value) {
        commit('destroyImage', value)
    },
    destroyUploadedImage({ commit }, value) {
        commit('destroyUploadedImage', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setTitle(state, value) {
        state.item.title = value
    },
    setDescription(state, value) {
        state.item.description = value
    },
    setStatus(state, value) {
        state.item.status = value
    },
    setClient_name(state, value) {
        state.item.client_name = value
    },
    setLocation(state, value) {
        state.item.location = value
    },
    setImage(state, value) {
        for (let i in value) {
            let image = value[i];
            if (typeof image === "object") {
                state.item.image.push(image);
            }
        }
    },
    destroyImage(state, value) {
        for (let i in state.item.image) {
            if (i == value) {
                state.item.image.splice(i, 1);
            }
        }
    },
    destroyUploadedImage(state, value) {
        for (let i in state.item.uploaded_image) {
            let data = state.item.uploaded_image[i];
            if (data.id === value) {
                state.item.uploaded_image.splice(i, 1);
            }
        }
    },
    
    setStatusEnum(state, value) {
        state.statusEnum = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
