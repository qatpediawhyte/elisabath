function initialState() {
    return {
        item: {
            id: null,
            title: null,
            logo: null,
            description: null,
            type: null,
        },
        
        typeEnum: [ { value: 'audio visual', label: 'Audio visual' }, { value: 'hospitality', label: 'Hospitality' }, { value: 'control rooms and studio', label: 'Control rooms and studio' }, ],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    
    typeEnum: state => state.typeEnum,
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (state.item.logo === null) {
                params.delete('logo');
            }
            if (! _.isEmpty(state.item.type) && typeof state.item.type === 'object') {
                params.set('type', state.item.type.value)
            }

            axios.post('/api/v1/brands', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (state.item.logo === null) {
                params.delete('logo');
            }
            if (! _.isEmpty(state.item.type) && typeof state.item.type === 'object') {
                params.set('type', state.item.type.value)
            }

            axios.post('/api/v1/brands/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/brands/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        
    },
    
    setTitle({ commit }, value) {
        commit('setTitle', value)
    },
    setLogo({ commit }, value) {
        commit('setLogo', value)
    },
    
    setDescription({ commit }, value) {
        commit('setDescription', value)
    },
    setType({ commit }, value) {
        commit('setType', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setTitle(state, value) {
        state.item.title = value
    },
    setLogo(state, value) {
        state.item.logo = value
    },
    setDescription(state, value) {
        state.item.description = value
    },
    setType(state, value) {
        state.item.type = value
    },
    
    setTypeEnum(state, value) {
        state.typeEnum = value
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
