function initialState() {
    return {
        item: {
            id: null,
            title: null,
            icon: null,
            image: null,
            description: null,
            front_image: null,
            brand: [],
        },
        brandsAll: [],
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    brandsAll: state => state.brandsAll,
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (state.item.icon === null) {
                params.delete('icon');
            }
            if (state.item.image === null) {
                params.delete('image');
            }
            if (state.item.front_image === null) {
                params.delete('front_image');
            }
            if (_.isEmpty(state.item.brand)) {
                params.delete('brand')
            } else {
                for (let index in state.item.brand) {
                    params.set('brand['+index+']', state.item.brand[index].id)
                }
            }

            axios.post('/api/v1/solutions', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (state.item.icon === null) {
                params.delete('icon');
            }
            if (state.item.image === null) {
                params.delete('image');
            }
            if (state.item.front_image === null) {
                params.delete('front_image');
            }
            if (_.isEmpty(state.item.brand)) {
                params.delete('brand')
            } else {
                for (let index in state.item.brand) {
                    params.set('brand['+index+']', state.item.brand[index].id)
                }
            }

            axios.post('/api/v1/solutions/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/solutions/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchBrandsAll')
    },
    fetchBrandsAll({ commit }) {
        axios.get('/api/v1/brands')
            .then(response => {
                commit('setBrandsAll', response.data.data)
            })
    },
    setTitle({ commit }, value) {
        commit('setTitle', value)
    },
    setIcon({ commit }, value) {
        commit('setIcon', value)
    },
    
    setImage({ commit }, value) {
        commit('setImage', value)
    },
    
    setDescription({ commit }, value) {
        commit('setDescription', value)
    },
    setFront_image({ commit }, value) {
        commit('setFront_image', value)
    },
    
    setBrand({ commit }, value) {
        commit('setBrand', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setTitle(state, value) {
        state.item.title = value
    },
    setIcon(state, value) {
        state.item.icon = value
    },
    setImage(state, value) {
        state.item.image = value
    },
    setDescription(state, value) {
        state.item.description = value
    },
    setFront_image(state, value) {
        state.item.front_image = value
    },
    setBrand(state, value) {
        state.item.brand = value
    },
    setBrandsAll(state, value) {
        state.brandsAll = value
    },
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
